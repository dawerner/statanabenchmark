import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--n_events",
        action="append",
        default=[],
        type=int,
        help="Number of events to use in benchmarks",
    )
    parser.addoption(
        "--model_name",
        action="append",
        default=[],
        help="Which model to benchmark",
        choices=("gauss_2", "gauss_18", "simultaneous"),
    )
    parser.addoption(
        "--n_channels",
        action="append",
        default=[],
        type=int,
        help="Number of channels in a simultaneous fit",
    )
    parser.addoption(
        "--backend_RooFit",
        action="append",
        default=[],
        help="list of backends to use in RooFit benchmarks",
    )
    parser.addoption(
        "--device_name",
        action="append",
        default=[],
        help="list of devices (CPUs, GPUs) to use in zfit benchmarks",
    )
    parser.addoption(
        "--use_grad",
        action="append",
        default=[],
        help="Whether to obtain the gradient from tensorflow or not (minuit) in zfit benchmarks",
    )
    parser.addoption(
        "--use_graph",
        action="append",
        default=[],
        type=int,
        help="Whether to obtain the graph in zfit",
    )

def pytest_generate_tests(metafunc):
    if "n_events" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("n_events")
        metafunc.parametrize("n_events", arg_value_list, ids=[f"N_Events={arg_value}" for arg_value in arg_value_list])
    if "model_details" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("model_name")
        tuple_list = []
        for arg_value in arg_value_list:
            if arg_value == "simultaneous":
                n_channels_list = metafunc.config.getoption("n_channels")
                for n_channels in n_channels_list:
                    tuple_list.append((arg_value, n_channels))
            else:
                tuple_list.append((arg_value, None))
        metafunc.parametrize("model_details", tuple_list, ids=[f"model={tuple[0]}{f'_{tuple[1]}' if tuple[1] is not None else ''}" for tuple in tuple_list])
#    if "n_channels" in metafunc.fixturenames:
#        arg_value_list = metafunc.config.getoption("n_channels")
#        metafunc.parametrize("n_channels", arg_value_list, ids=[f"N_Channels={arg_value}" for arg_value in arg_value_list])
    if "backend_RooFit" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("backend_RooFit")
        metafunc.parametrize("backend_RooFit", arg_value_list, ids=["RooFit_"+arg_value for arg_value in arg_value_list])
    if "device_name" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("device_name")
        metafunc.parametrize("device_name", arg_value_list, ids=["zfit_"+arg_value for arg_value in arg_value_list])
    if "use_grad" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("use_grad")
        metafunc.parametrize("use_grad", arg_value_list, ids=[f"use_grad={arg_value}" for arg_value in arg_value_list])
    if "use_graph" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("use_graph")
        metafunc.parametrize("use_graph", arg_value_list, ids=[f"use_graph={arg_value}" for arg_value in arg_value_list])
