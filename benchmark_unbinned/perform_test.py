out_name = 'benchmark_zfit'
histogram_name = None #'./.benchmarks/benchmark_'`date +%F_%H%M`
group_by = None #'param:n_events'
timer = None #'time.process_time'
warmup_it = None

n_events_list = [
    2**7,
#    2**8,
    2**9,
#    2**10,
    2**11,
#    2**12,
    2**13,
#    2**14,
    2**15,
#    2**16,
    2**17,
#    2**18,
    2**19,
#    2**20,
    2**21,
#    2**22,
]
model_list = [
    "gauss_2",
    "gauss_18",
    "simultaneous",
]
n_channels_list = [
    2,
    3
]
backend_RooFit_list = [
    'legacy',
    'cpu',
    'codegen',
#    'cuda'
]
device_name_list = [
    'CPU:0',
#    'GPU:0'
]
use_grad_list = [
    'grad',
    'no_grad'
]
use_graph_list = [
    0,
    1
]

def get_command():
    command = 'python -m pytest'
    if out_name is not None:
        command += ' --benchmark-save='+out_name
    if histogram_name is not None:
        command += ' --benchmark-histogram='+histogram_name
    if group_by is not None:
        command += ' --benchmark-group-by='+group_by
    if timer is not None:
        command += ' --benchmark-timer='+timer
    if warmup_it is not None:
        command += ' --benchmark-warmup=on'
        command += f' --benchmark-warmup-iterations={warmup_it}'
    for n_events in n_events_list:
        command += f' --n_events={n_events}'
    for model in model_list:
        command += f' --model_name='+model
    for n_channels in n_channels_list:
        command += f' --n_channels={n_channels}'
    for backend in backend_RooFit_list:
        command += ' --backend_RooFit='+backend
    for name in device_name_list:
        command += ' --device_name='+name
    for use_grad in use_grad_list:
        command += ' --use_grad='+use_grad
    for use_graph in use_graph_list:
        command += f' --use_graph={use_graph}'
    return command

def main():
    import os
    command = get_command()
    os.system(command)

if __name__ == "__main__":
    main()
