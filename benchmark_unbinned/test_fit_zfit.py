import os
os.environ["ZFIT_DISABLE_TF_WARNINGS"] = "1"
import zfit
zfit.settings.changed_warnings.all = False

def get_data_2_params(observable, n_events):
    import numpy as np
    import zfit
    np.random.seed(0)

    data_np = np.random.normal(loc=1, size=n_events)
    data = zfit.Data.from_numpy(obs=observable, array=data_np)

    return data

def get_data_18_params(observable, n_events):
    import numpy as np
    import zfit
    np.random.seed(0)

    data_list = []

    for i in range(9):
        data_list.append(np.random.normal(loc=2.5+5*i, size=n_events//9))

    data_np = np.array(data_list).flatten()
    data = zfit.Data.from_numpy(obs=observable, array=data_np)

    return data

def get_data_simultaneous(observable, n_channels, n_events, limits):
    import numpy as np
    import zfit
    np.random.seed(0)

    fraction=0.9
    slope=0.005
    fract = sum(np.random.binomial(1, 1 - fraction, n_events) == 0)

    data_list = []
    for channel in range(n_channels):
        bkg = np.random.exponential(1/slope, n_events // n_channels)
        sig = np.random.normal(3096.916, 12, fract)
        tot = np.concatenate([sig, bkg])
        data_list.append(tot)

    data = [zfit.Data.from_numpy(obs=observable, array=observable.filter(dataset)) for dataset in data_list]

    return data

def get_model_2_params(observable, suffix="param"):
    import zfit
    mu = zfit.Parameter(f"mu_stack_{suffix}", 2.4, -1, 5)
    sigma = zfit.Parameter(f"sigma_stack_{suffix}", 1.3, 0, 5)
    gauss_list = []
    frac_list = []
    for i in range(9):
        gauss_list.append(zfit.pdf.Gauss(obs=observable, mu=mu, sigma=sigma))
        frac_list.append(1/9)

    gauss = zfit.pdf.SumPDF(gauss_list, frac_list[:-1], observable)
    return gauss

def get_model_18_params(observable, suffix="param"):
    import zfit

    gauss_list = []
    frac_list = []

    for i in range(9):
        mu = zfit.Parameter(f"mu_peaks_{i}_{suffix}", 1.9+5*i, -1+5*i, 6+5*i)
        sigma = zfit.Parameter(f"sigma_peaks_{i}_{suffix}", 1.3, 0, 5)
        gauss_list.append(zfit.pdf.Gauss(obs=observable, mu=mu, sigma=sigma))
        frac_list.append(1/9)

    gauss = zfit.pdf.SumPDF(gauss_list, frac_list[:-1], observable)
    return gauss

def get_model_simultaneous(observable, channel, limits, suffix="param"):
    mu = zfit.Parameter(f"mu_simult_{channel}_"+suffix, 3100, limits[0], limits[1])
    sigma = zfit.Parameter(f"sigma_simult_{channel}_"+suffix, 10, 1, 30)
    gauss = zfit.pdf.Gauss(obs=observable, mu=mu, sigma=sigma)

    slope = zfit.Parameter(f"slope_simult_{channel}_"+suffix, -0.002, -0.05, 0.0)
    exp = zfit.pdf.Exponential(lambda_=slope, obs=observable)

    Nsig = zfit.Parameter(f"Nsig_simult_{channel}_"+suffix, 1000, 0, 1000000)
    Nbkg = zfit.Parameter(f"Nbkg_simult_{channel}_"+suffix, 1000, 0, 2000000)
    ext_gauss = gauss.create_extended(Nsig)
    ext_exp = exp.create_extended(Nbkg)

    pdf = zfit.pdf.SumPDF([ext_exp, ext_gauss])
    return pdf

def fit_gauss(model, data, use_grad, is_simultaneous, verbosity=0):
    import zfit

    # build the loss
    if is_simultaneous:
        nll = zfit.loss.ExtendedUnbinnedNLL(model=model, data=data)
        nll.value_gradient(params=list(nll.get_params()))
    else:
        nll = zfit.loss.UnbinnedNLL(model=model, data=data)

    # minimize
    minimizer = zfit.minimize.Minuit(verbosity=verbosity, tol=1e-3, mode=0, gradient=use_grad)
    result = minimizer.minimize(nll)

    # calculate errors
    param_errors = result.hesse()

    return result, param_errors

def test_fit_zfit(benchmark, device_name, use_grad, use_graph, n_events, model_details):
    import zfit
    zfit.run.set_mode(graph=bool(use_graph))
    import tensorflow as tf
    with tf.device(device_name):
        if model_details[0] == "gauss_2":
            observable = zfit.Space('x', limits=(-10, 10))
            data = get_data_2_params(observable, n_events)
            model = get_model_2_params(observable, f"{device_name}_{use_grad}_{use_graph}_{n_events}")
            params = model.get_params()
            initvals = [param.value() for param in params]
        elif model_details[0] == "gauss_18":
            observable = zfit.Space('x', limits=(-10, 60))
            data = get_data_18_params(observable, n_events)
            model = get_model_18_params(observable, f"{device_name}_{use_grad}_{use_graph}_{n_events}")
            params = model.get_params()
            initvals = [param.value() for param in params]
        elif model_details[0] == "simultaneous":
            n_channels = model_details[1]
            fit_range = (2900, 3300)
            observable = zfit.Space('x', limits=fit_range)
            data = get_data_simultaneous(observable, n_channels, n_events, fit_range)
            model = [get_model_simultaneous(observable, i, fit_range, f"{device_name}_{use_grad}_{use_graph}_{n_events}_{n_channels}") for i in range(n_channels)]
            params = [parameter for model_channel in model for parameter in model_channel.get_params()]
            initvals = [param.value() for param in params]

        result, errors = benchmark(fit_gauss, model, data, use_grad=="grad", model_details[0]=="simultaneous")

        if model_details[0] == "gauss_2":
            benchmark.extra_info["mu"] = result.params[f"mu_stack_{device_name}_{use_grad}_{use_graph}_{n_events}"]["value"]
            benchmark.extra_info["mu_err"] = result.params[f"mu_stack_{device_name}_{use_grad}_{use_graph}_{n_events}"]["hesse"]["error"]
        elif model_details[0] == "gauss_18":
            benchmark.extra_info["mu"] = result.params[f"mu_peaks_0_{device_name}_{use_grad}_{use_graph}_{n_events}"]["value"]
            benchmark.extra_info["mu_err"] = result.params[f"mu_peaks_0_{device_name}_{use_grad}_{use_graph}_{n_events}"]["hesse"]["error"]
        elif model_details[0] == "simultaneous":
            benchmark.extra_info["mu"] = result.params[f"mu_simult_0_{device_name}_{use_grad}_{use_graph}_{n_events}_{n_channels}"]["value"]
            benchmark.extra_info["mu_err"] = result.params[f"mu_simult_0_{device_name}_{use_grad}_{use_graph}_{n_events}_{n_channels}"]["hesse"]["error"]

        zfit.param.set_values(params, initvals)
#        data.reset_cache_self()
#        model.reset_cache_self()

def main(device_name, use_grad):
    import zfit
    import tensorflow as tf
    with tf.device(device_name):
        observable = zfit.Space('x', limits=(-10, 60))
        n_events = int(1e5)
        data = get_data_18_params(observable, n_events)
        model = get_model_18_params(observable)
        #for param in model.get_params():
        #    print(param.value(), param.lower, param.upper)

        result, errors = fit_gauss(model, data, use_grad, False, 8)
        print(f"\nResulting parameters:\n{result}")
        print(result.params["mu_peaks_0_param"]["value"])

if __name__ == "__main__":
    main("CPU:0", True)
