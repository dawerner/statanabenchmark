def get_data_2_params(observable, n_events):
    import numpy as np
    import ROOT
    np.random.seed(0)

    data_np = np.random.normal(loc=1.0, size=n_events)
    data = ROOT.RooDataSet.from_numpy({"x": data_np}, [observable])

    return data

def get_data_18_params(observable, n_events):
    import numpy as np
    import ROOT
    np.random.seed(0)

    data_list = []

    for i in range(9):
        data_list.append(np.random.normal(loc=2.5+5*i, size=n_events//9))

    data_np = np.array(data_list).flatten()
    data = ROOT.RooDataSet.from_numpy({"x": data_np}, [observable])

    return data

def get_data_simultaneous(observable, n_channels, n_events, limits):
    import numpy as np
    import ROOT
    np.random.seed(0)

    fraction=0.9
    slope=0.005
    fract = sum(np.random.binomial(1, 1 - fraction, n_events) == 0)

    index = ROOT.RooCategory("index", "index")
    data_list = []
    for channel in range(n_channels):
        bkg = np.random.exponential(1/slope, n_events // n_channels)
        sig = np.random.normal(3096.916, 12, fract)
        tot = np.concatenate([sig, bkg])
        data_list.append(tot)
        index.defineType(f"type_{channel}")

    print("matching efficiency = ", fract / n_events)
    data = [ROOT.RooDataSet.from_numpy({"x":dataset}, [observable]) for dataset in data_list]

    # define the combined set
    combData = ROOT.RooDataSet("combData", "combined data", ROOT.RooArgSet(observable), ROOT.RooFit.Index(index), *[ROOT.RooFit.Import(f"type_{channel}", data[channel]) for channel in range(n_channels)])
    combData.Print("v")

    return combData, index

def get_model_2_params(observable):
    import ROOT
    mu = ROOT.RooRealVar("mu", "", 2.4, -1, 5)
    sigma = ROOT.RooRealVar("sigma", "", 1.3, 0, 5)
    gauss_list = []
    frac_list = []
    for i in range(9):
        gauss_list.append(ROOT.RooGaussian(f"gauss_{i}", "", observable, mu, sigma))
        frac_list.append(ROOT.RooConstVar(f"frac_{i}", "", 1/9))

    gauss = ROOT.RooAddPdf("gauss", "", gauss_list, frac_list[:-1])
    gauss.keepAlive = (gauss_list, frac_list[:-1])
    return (mu, sigma), gauss

def get_model_18_params(observable):
    import ROOT

    mu_list = []
    sigma_list = []
    gauss_list = []
    frac_list = []

    for i in range(9):
        mu_list.append(ROOT.RooRealVar(f"mu_{i}", "", 1.9+5*i, -1+5*i, 6+5*i))
        sigma_list.append(ROOT.RooRealVar(f"sigma_{i}", "", 1.3, 0, 5))
        gauss_list.append(ROOT.RooGaussian(f"gauss_{i}", "", observable, mu_list[-1], sigma_list[-1]))
        frac_list.append(ROOT.RooConstVar(f"frac_{i}", "", 1/9))

    gauss = ROOT.RooAddPdf("gauss", "", gauss_list, frac_list[:-1])
    gauss.keepAlive = (gauss_list, frac_list[:-1])
    return (mu_list, sigma_list), gauss

def get_model_simultaneous(observable, n_channels, limits, index):
    import ROOT
    
    mu = ROOT.RooRealVar("mu", "", 3100, limits[0], limits[1])
    sigma = ROOT.RooRealVar("sigma", "", 10, 1, 30)
    slope = ROOT.RooRealVar("slope", "", -0.002, -0.05, 0.0)

    gauss_list = []
    exp_list = []
    nsig_list = []
    nbkg_list = []
    pdf_list = []

    for channel in range(n_channels):
        gauss_list.append(ROOT.RooGaussian(f"gauss_{channel}", "", observable, mu, sigma))
        exp_list.append(ROOT.RooExponential(f"exp_{channel}", "", observable, slope))

        # define yields
        nsig_list.append(ROOT.RooRealVar(f"nsig_{channel}", "", 1000, 0., 1000000))
        nbkg_list.append(ROOT.RooRealVar(f"nbkg_{channel}", "", 1000, 0, 2000000))

        # sum pdfs
        pdf_list.append(ROOT.RooAddPdf(f"model_{channel}", "", ROOT.RooArgList(exp_list[-1], gauss_list[-1]), ROOT.RooArgList(nbkg_list[-1], nsig_list[-1])))

    simPdf = ROOT.RooSimultaneous("simPdf", "simultaneous pdf", index)
    for channel in range(n_channels):
        simPdf.addPdf(pdf_list[channel], f"type_{channel}")

    simPdf.keepAlive = (gauss_list, exp_list, pdf_list)
    return (mu, sigma, slope, nsig_list, nbkg_list), simPdf

def fit_gauss(model, data, backend):
    import ROOT

    # build the loss
    nll = model.createNLL(data, EvalBackend=backend)
    ROOT.SetOwnership(nll, True)

    # minimize
    minimizer = ROOT.RooMinimizer(nll)
    minimizer.setStrategy(0)
    minimizer.minimize("Minuit2", "")

    # calculate errors
    minimizer.hesse()

    result = minimizer.save()
    ROOT.SetOwnership(result, True)
    return result

def test_fit_RooFit(benchmark, backend_RooFit, n_events, model_details):
    import ROOT

    if model_details[0] == "gauss_2":
        observable = ROOT.RooRealVar("x", "x", 0, -10, 10)
        data = get_data_2_params(observable, n_events)
        parameters, model = get_model_2_params(observable)
    elif model_details[0] == "gauss_18":
        observable = ROOT.RooRealVar("x", "x", 0, -10, 60)
        data = get_data_18_params(observable, n_events)
        parameters, model = get_model_18_params(observable)
    elif model_details[0] == "simultaneous":
        n_channels = model_details[1]
        fit_range = (2900, 3300)
        observable = ROOT.RooRealVar("x", "x", fit_range[0], fit_range[1])
        data, index = get_data_simultaneous(observable, n_channels, n_events, fit_range)
        parameters, model = get_model_simultaneous(observable, n_channels, fit_range, index)

    result = benchmark(fit_gauss, model, data, backend_RooFit)

    if model_details[0] == "gauss_2":
        benchmark.extra_info["mu"] = parameters[0].getVal()
        benchmark.extra_info["mu_err"] = parameters[0].getError()
    elif model_details[0] == "gauss_18":
        benchmark.extra_info["mu"] = parameters[0][0].getVal()
        benchmark.extra_info["mu_err"] = parameters[0][0].getError()
    elif model_details[0] == "simultaneous":
        benchmark.extra_info["mu"] = parameters[0].getVal()
        benchmark.extra_info["mu_err"] = parameters[0].getError()

def main():
    import ROOT
    observable = ROOT.RooRealVar("x", "x", 0, -2, 47)
    n_events = int(5e4)
    data = get_data_18_params(observable, n_events)
    parameters, model = get_model_18_params(observable)

    for param in parameters[0]+parameters[1]:
        param.Print()

    result = fit_gauss(model, data, "cuda")

    c = ROOT.TCanvas("", "", 1800, 600)
    frame = observable.frame()
    data.plotOn(frame)
    model.plotOn(frame)
    frame.SetTitle("")
    frame.Draw()
    c.Print("RooFitPlot.pdf")
    c.Print("RooFitPlot.png")

if __name__ == "__main__":
    main()
