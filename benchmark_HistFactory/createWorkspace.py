import numpy as np
import itertools as it
import json

def pyhf_write_sample(name, data, uncertainty=None, normfactor=None):
    has_uncertainty = False
    has_normfactor = False

    string_name = f"\"name\":\"{name}\""                    # Sample must contain
    string_data = f"\"data\":[{','.join(map(str,data))}]"   #  name and data
    string_modifiers = "\"modifiers\":["                    # Modifiers can be empty

    if uncertainty is not None:
        has_uncertainty = True
        uncert_name, uncert_data = uncertainty
        string_uncertainty = f"{{\"name\":\"{uncert_name}\",\"type\":\"shapesys\",\"data\":[{','.join(map(str,uncert_data))}]}}"

    if normfactor is not None:
        has_normfactor = True
        string_normfactor = f"{{\"name\":\"{normfactor}\",\"type\":\"normfactor\",\"data\":null}}"

    # Add the applicable modifiers to the string
    if has_uncertainty:
        string_modifiers += string_uncertainty
    if has_uncertainty and has_normfactor:
        string_modifiers += ","
    if has_normfactor:
        string_modifiers += string_normfactor
    string_modifiers += "]"

    # Construct the complete string describing the sample
    sample = "{"+string_name+","+string_data+","+string_modifiers+"}"
    return sample

def pyhf_write_channel(name, samples):
    channel = f"{{\"name\":\"{name}\",\"samples\":[" # Channel has a name and at least one sample
    for sample in samples:
        channel += sample+","  # The sample strings are already complete and just need to be appended
    channel = channel[:-1]          # Remove last "," from string
    channel += "]}"             # Close list of samples and the {}-brackets around the channel
    return channel

def pyhf_write_observation(channel_name, data):
    string_name = f"\"name\":\"{channel_name}\""            # Observation is always connected to an existing channel
    string_data = f"\"data\":[{','.join(map(str,data))}]"   # The data should be obtained from the signal and background distributions for a convergent fit

    observation = "{"+string_name+","+string_data+"}"
    return observation

def pyhf_write_spec(channels, observations):
    channel_strings = []
    observation_strings = []
    for channel_name in channels:
        samples = channels[channel_name]
        sample_strings = []
        for sample_name in samples:
            sample_data = samples[sample_name]["data"]
            sample_modifiers = samples[sample_name]["modifiers"]
            sample_strings.append(pyhf_write_sample(sample_name, sample_data, sample_modifiers["uncertainty"], sample_modifiers["normfactor"]))

        channel_strings.append(pyhf_write_channel(channel_name, sample_strings))
        observation_strings.append(pyhf_write_observation(channel_name, observations[channel_name]["observation"]))

    spec = "{\"channels\":["
    for string in channel_strings:
        spec += string+","
    spec = spec[:-1]
    spec += "],\"version\":\"1.0.0\",\"measurements\":[{\"name\":\"Measurement\",\"config\":{\"poi\":\"mu\",\"parameters\":[]}}],\"observations\":["
    for string in observation_strings:
        spec += string+","
    spec = spec[:-1]
    spec += "]}"

    return spec

def ROOT_write_sample(name, data, uncertainty=None, normfactor=None):
    has_uncertainty = False
    has_normfactor = False
    
    if uncertainty is not None:
        has_uncertainty = True
        uncert_name, uncert_data = uncertainty
        string_uncertainty = f"{{\"name\":\"{uncert_name}\",\"type\":\"staterror\",\"constraint\":\"Poisson\"}}"

    if normfactor is not None:
        has_normfactor = True
        string_normfactor = f"{{\"name\":\"{normfactor}\",\"type\":\"normfactor\",\"parameter\":\"{normfactor}\"}}"

    string_name = f"\"name\":\"{name}\""
    string_data = f"\"data\":{{\"contents\":[{','.join(map(str,data))}]"
    if has_uncertainty:
        string_data += f",\"errors\":[{','.join(map(str,uncert_data))}]"
    string_data += "}"
    string_modifiers = "\"modifiers\":["
    
    # Add the applicable modifiers to the string
    if has_uncertainty:
        string_modifiers += string_uncertainty
    if has_uncertainty and has_normfactor:
        string_modifiers += ","
    if has_normfactor:
        string_modifiers += string_normfactor
    string_modifiers += "]"
    
    # Construct the complete string describing the sample
    sample = "{"+string_name+","+string_data+","+string_modifiers+"}"
    return sample

def ROOT_write_axes(name, bins):
    string_axes = f"{{\"name\":\"obs_x_{name}\",\"max\":{bins},\"min\":0,\"nbins\":{bins}}}"
    return string_axes

def ROOT_write_channel(name, samples, axes):
    channel = f"{{\"name\":\"{name}\",\"type\":\"histfactory_dist\",\"axes\":["+axes+"],\"samples\":[" # Channel (or distribution) has a name and at least one sample
    for sample in samples:
        channel += sample+","   # The sample strings are already complete and just need to be appended
    channel = channel[:-1]      # Remove last "," from string
    channel += "]}"             # Close list of samples and the {}-brackets around the channel
    return channel

def ROOT_write_observation(channel_name, data, axes):
    string_name = f"\"name\":\"{channel_name}\""
    string_type = "\"type\":\"binned\""
    string_axes = "\"axes\":["+axes+"]"
    string_data = f"\"contents\":[{','.join(map(str,data))}]"
    
    observation = "{"+string_name+","+string_type+","+string_axes+","+string_data+"}"
    return observation

def ROOT_write_spec(channels, observations, bins_list):
    channel_strings = []
    observation_strings = []
    for channel_name, bins in zip(channels, bins_list):
        samples = channels[channel_name]
        sample_strings = []
        for sample_name in samples:
            sample_data = samples[sample_name]["data"]
            sample_modifiers = samples[sample_name]["modifiers"]
            sample_strings.append(ROOT_write_sample(sample_name, sample_data, sample_modifiers["uncertainty"], sample_modifiers["normfactor"]))

        axes_string = ROOT_write_axes(channel_name, bins)
        channel_strings.append(ROOT_write_channel(channel_name, sample_strings, axes_string))
        observation_strings.append(ROOT_write_observation("obsData_"+channel_name, observations[channel_name]["observation"], axes_string))
        observation_strings.append(ROOT_write_observation("asimovData_"+channel_name, observations[channel_name]["asimov"], axes_string))

    spec = "{\"distributions\":["
    for string in channel_strings:
        spec += string+","
    spec = spec[:-1]
    spec += "],\"data\":["
    for string in observation_strings:
        spec += string+","
    spec = spec[:-1]
    spec += "],\"likelihoods\":[{\"name\":\"simPdf_obsData\",\"distributions\":["
    for channel_name in channels:
        spec += f"\"{channel_name}\","
    spec = spec[:-1]
    spec += "],\"data\":["
    for channel_name in channels:
        spec += f"\"obsData_{channel_name}\","
    spec = spec[:-1]
    spec += "]}],\"analyses\":[{\"name\":\"simPdf_obsData\",\"likelihood\":\"simPdf_obsData\",\"parameters_of_interest\":[\"mu\"]}],"
    spec += "\"metadata\":{\"hs3_version\":\"0.1.90\"},"
    spec += "\"misc\":{\"ROOT_internal\":{\"ModelConfigs\":{\"simPdf_obsData\":{\"mcName\":\"ModelConfig\",\"pdfName\":\"simPdf\"}},"
    spec += f"\"combined_datas\":{{\"obsData\":{{\"index_cat\":\"channelCat\",\"indices\":{list(range(len(channels)))},\"labels\":["
    for channel_name in channels:
        spec += f"\"{channel_name}\","
    spec = spec[:-1]
    spec += f"]}},\"asimovData\":{{\"index_cat\":\"channelCat\",\"indices\":{list(range(len(channels)))},\"labels\":["
    for channel_name in channels:
        spec += f"\"{channel_name}\","
    spec = spec[:-1]
    spec += "]}},\"combined_distributions\":{\"simPdf\":{\"distributions\":["
    for channel_name in channels:
        spec += f"\"{channel_name}\","
    spec = spec[:-1]
    spec += f"],\"index_cat\":\"channelCat\",\"indices\":{list(range(len(channels)))},\"labels\":["
    for channel_name in channels:
        spec += f"\"{channel_name}\","
    spec = spec[:-1]
    spec += "]}}}}}"

    return spec

def create_spec(names, shapes):
    rng = np.random.default_rng(0)
    channels = {}
    observations = {}
    for (bins, (Nsig, Nbkg)), channel_name in zip(shapes, names):
        signal = 10*rng.random((Nsig, bins))
        background = 10*rng.random((Nbkg, bins))
        uncertainty = np.sqrt(background) * np.abs(rng.normal(1, 0.2, (Nbkg, bins)))
        observation = rng.poisson(background, (Nbkg, bins)).sum(0)

        sample_names = names[channel_name]
        data = {name:data for name, data in zip(sample_names, it.chain(signal, background))}

        modifiers = {}
        for i, name in enumerate(sample_names):
            if i < Nsig:
                modifiers[name] = {"uncertainty":None, "normfactor":"mu"}
            else:
                modifiers[name] = {"uncertainty":(name+"_uncert",uncertainty[i-Nsig]), "normfactor":None}

        channel = {key:{"data":data[key], "modifiers":modifiers[key]} for key in data}
        channels[channel_name] = channel

        observations[channel_name] = {"observation":observation, "asimov":background.sum(0)}

    return channels, observations

def main():
    from pathlib import Path
    Path("./models").mkdir(parents=True, exist_ok=True)
    with open("workspace_config.json") as f:
        ws_dict = json.load(f)
    for workspace_name, workspace in ws_dict.items():
        names = workspace["names"]
        shapes = workspace["shapes"]
        bins = [tup[0] for tup in shapes]
        channels, observations = create_spec(names, shapes)

        spec_pyhf = pyhf_write_spec(channels, observations)
        spec_ROOT = ROOT_write_spec(channels, observations, bins)

        with open("./models/"+workspace_name+"_pyhf.json", "w") as f:
            f.write(spec_pyhf)
        with open("./models/"+workspace_name+"_ROOT.json", "w") as f:
            f.write(spec_ROOT)

if __name__ == "__main__":
    main()
