import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--workspace",
        action="append",
        default=[],
        help="list of workspaces to pass to benchmarks",
    )
    parser.addoption(
        "--backend_pyhf",
        action="append",
        default=[],
        help="list of backends to use in pyhf benchmarks",
    )
    parser.addoption(
        "--backend_RooFit",
        action="append",
        default=[],
        help="list of backends to use in RooFit benchmarks",
    )

def pytest_generate_tests(metafunc):
    if "workspace" in metafunc.fixturenames:
        metafunc.parametrize("workspace", metafunc.config.getoption("workspace"))
    if "backend_pyhf" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("backend_pyhf")
        metafunc.parametrize("backend_pyhf", arg_value_list, ids=["pyhf_"+arg_value for arg_value in arg_value_list])
    if "backend_RooFit" in metafunc.fixturenames:
        arg_value_list = metafunc.config.getoption("backend_RooFit")
        metafunc.parametrize("backend_RooFit", arg_value_list, ids=["RooFit_"+arg_value for arg_value in arg_value_list])
