def read_workspace(filename):
    import json
    import pyhf
    with open(filename) as f:
        spec = json.load(f)

    workspace = pyhf.Workspace(spec)
    model = workspace.model()
    data = workspace.data(model)

    return model, data

def hypotest(data, pdf):
    import pyhf
    return pyhf.infer.hypotest(1.0, data, pdf, test_stat="qtilde", return_expected=True)

def test_hypotest(benchmark, backend_pyhf, workspace):
    import pyhf
    from opt_own_minuit import own_minuit_optimizer
    pyhf.set_backend(backend_pyhf, own_minuit_optimizer(do_hesse=False, strategy=0))
    model, data = read_workspace(workspace+"_pyhf.json")

    result = benchmark(hypotest, data, model)

    current_backend = pyhf.get_backend()[0]
    if type(current_backend) == pyhf.tensor.tensorflow_backend:
        CLs_obs = result[0].numpy().item()
        CLs_exp = result[1].numpy().item()
    else:
        CLs_obs = result[0].item()
        CLs_exp = result[1].item()

    benchmark.extra_info["CLs_obs"] = CLs_obs
    benchmark.extra_info["CLs_exp"] = CLs_exp

def main(workspace):
    import pyhf
    model, data = read_workspace(workspace+"_pyhf.json")

    result = hypotest(data, model)

    current_backend = pyhf.get_backend()[0]
    if type(current_backend) == pyhf.tensor.tensorflow_backend:
        CLs_obs = result[0].numpy().item()
        CLs_exp = result[1].numpy().item()
    else:
        CLs_obs = result[0].item()
        CLs_exp = result[1].item()

    print(f"Observed: {CLs_obs}, Expected: {CLs_exp}")


if __name__ == "__main__":
    import pyhf
    from opt_own_minuit import own_minuit_optimizer
    pyhf.set_backend("numpy", own_minuit_optimizer(do_hesse=False, strategy=0))
    main("./models/workspace42")
