import ROOT

# As long as this object lives, RooFits output below WARNING level is silenced
changeMsgLvl = ROOT.RooHelpers.LocalChangeMsgLevel(ROOT.RooFit.WARNING)

def read_workspace(config):
    import ROOT

    # start by creating an empty workspace
    ws = ROOT.RooWorkspace("workspace")

    # the RooJSONFactoryWSTool is responsible for importing and exporting things to and from your workspace
    tool = ROOT.RooJSONFactoryWSTool(ws)

    # use it to import the information from your JSON file
    tool.importJSON(config)

    return ws

def minimize(nll):
    minim = ROOT.RooMinimizer(nll)

    minim.setPrintLevel(-1)
    minim.setStrategy(0)
    minim.setEps(0.1)

    minim.minimize("Minuit2", "")  # Perform the fit with the minimizer "minuit"

    results = minim.save()
    return results

def get_nll_mu(model, data, poi, poi_value, alt_nom_gammas, backend):
    """
    Fit the pdf to data to get the NLL value and the best-fit value of the parameter of interest.
    The parameter of interest (poi) is either floating and will be optimized to the best estimate,
    or if poi_value is not None, it will be fixed to this value.
    """
    import ROOT

    params = model.GetPdf().getParameters(data)
    gammas = model.GetNuisanceParameters()
    init_params = params.snapshot()
    nom_gammas = model.GetGlobalObservables()
    init_nom_gammas = nom_gammas.snapshot()

    was_constant = poi.isConstant()
    old_value = poi.getVal()

    if poi_value is not None:   # If a fixed-poi fit is to be
        poi.setConstant(True)   #  performed, the fixed value
        poi.setVal(poi_value)   #  is set here.

    if alt_nom_gammas is not None:          # The constraint parameters from the post-fit asimov
        nom_gammas.assign(alt_nom_gammas)   #  generation are assigned here and must be passed to
                                            #  this function for all asimov fits.

    for param in gammas:
        if type(param) is not type(ROOT.RooCategory()):
            param.setRange(1e-10, 10)

    nll = model.createNLL(data, ROOT.RooFit.EvalBackend(backend))  # Get the negative log likelihood of
                                                                   #  finding the given data from the pdf

    # Check for floating parameters
    has_floating_params = False
    for param in params:
        if not param.isConstant():
            has_floating_params = True

    # No fit is necessary if all parameters are constant
    if has_floating_params == False:
        val = nll.getVal()    # The likelihood found for poi_value
        mu_hat = poi_value    # The resulting strength parameter mu is identical as no fit was performed
        poi.setConstant(was_constant)       # Reset all properties of the parameters
        poi.setVal(old_value)               #  to not affect future fits if this
        nom_gammas.assign(init_nom_gammas)  #  function is called multiple times
        return val, mu_hat

    minimize(nll)

    val = nll.getVal()     # Export the nll value of the best-fit parameters
    mu_hat = poi.getVal()  # Export the best-fit value of mu

    poi.setConstant(was_constant)
    poi.setVal(old_value)

    params.assign(init_params)
    nom_gammas.assign(init_nom_gammas)

    return val, mu_hat

def get_q_tilde(model, data, poi, alt_nom_gammas, backend):
    """
    Formula (16) from the paper https://arxiv.org/pdf/1007.1727.pdf is used as the
    definition of our test statistic q_mu_tilde.
    """
    nll_val_float, mu_hat = get_nll_mu(model, data, poi, None, alt_nom_gammas, backend)  # Get the likelihood from an unconstrained fit
    nll_val_fixed, _ = get_nll_mu(model, data, poi, poi.getVal(), alt_nom_gammas, backend)   # Get the likelihood with a fixed poi

    q_tilde = 2 * (nll_val_fixed - nll_val_float)  # Calculate q_tilde

    if mu_hat <= poi.getVal():  # Following (16), 0 is returned if the fit yields a negative signal strength
        return q_tilde
    return 0

def get_F(q_tilde, test_mu, sigma, mu_prime):
    """
    Formula (65) from the paper https://arxiv.org/pdf/1007.1727.pdf defines the cumulative probability
    density function of q_tilde. This in turned will be needed to compute the exclusion limits.
    The quantity sigma describes the width of the distribution of q_tilde and is found from equation (29)
    """
    from scipy.stats import norm

    phi = norm.cdf  # Phi is the cumulative density of the normal distribution

    if q_tilde <= test_mu**2 / sigma**2:
        return phi(q_tilde ** (0.5) - (test_mu - mu_prime) / sigma)
    else:
        return phi((q_tilde - (test_mu**2 - 2 * test_mu * mu_prime) / sigma**2) / (2 * test_mu / sigma))

def get_CLs(model, obs_data, asimov_data, mu, alt_nom_gammas, backend):
    """
    Get the CLs, which is p_{s+b}/(1 − p_b).
    """
    q_tilde_obs = get_q_tilde(model, obs_data, mu, None, backend)
    q_tilde_asimov = get_q_tilde(model, asimov_data, mu, alt_nom_gammas, backend)

    # Use the test statistic for the asimov dataset under the null hypothesis to find the width
    #  of the distributions according to (29).
    sigma = (mu.getVal()**2 / q_tilde_asimov) ** 0.5

    # Calculate the CL values for the hypotheses signal and background and background-only.
    # In both tests the null-hypothesis to be rejected is extracted from mu and set as its initial value.
    test_mu = mu.getVal()

    clsb_obs = 1.0 - get_F(q_tilde_obs, test_mu, sigma, test_mu)  # In the case of s and b the real strength is positive
    clb_obs = 1.0 - get_F(q_tilde_obs, test_mu, sigma, 0.0)  # In the b-only case the real strength is 0
    cls_obs = clsb_obs / clb_obs

    clsb_exp = 1.0 - get_F(q_tilde_asimov, test_mu, sigma, test_mu)  # In the case of s and b the real strength is positive
    clb_exp = 1.0 - get_F(q_tilde_asimov, test_mu, sigma, 0.0)  # In the b-only case the real strength is 0
    cls_exp = clsb_exp / clb_exp

    return cls_obs, cls_exp

def post_fit_asimov(observables, model, data, poi, poi_value, backend):
    """
    The Asimov values are generated "post-fit" by performing a fixed-poi fit of the pdf to
    the observed data to find the best-fit values for all other parameters. From these
    parameter values, the expectation values for all bins are taken as the asimov data.
    """
    from ROOT.RooStats import AsymptoticCalculator # Use this RooStats calculator for its GenerateAsimovData function

    params = model.GetPdf().getParameters(data) # The initial parameter values are saved in
    init_params = params.snapshot()             #  order to reset them after the fit

    was_constant = poi.isConstant()
    old_value = poi.getVal()

    poi.setConstant(True)   # The fit is performed with the poi fixed to
    poi.setVal(poi_value)   #  the value that is to be found by the test

    nll = model.createNLL(data, ROOT.RooFit.EvalBackend(backend))
    results = minimize(nll)

    asimov_gammas = results.floatParsFinal()                        # One variant of post-fit asimov data that
    asimov_nom_gammas = model.GetGlobalObservables().snapshot()     #  is also used by pyhf is to change the constraints
    for nom_gamma, gamma in zip(asimov_nom_gammas, asimov_gammas):  #  of all parameters to make the best-fit values the
        new_val = nom_gamma.getVal()*gamma.getVal()                 #  new default values. This is implemented by saving
        nom_gamma.setVal(new_val)                                   #  dataset and passing it to later fits to asimov.

    asimov_data = AsymptoticCalculator.GenerateAsimovData(model.GetPdf(), observables)

    poi.setConstant(was_constant)
    poi.setVal(old_value)
    params.assign(init_params)

    return asimov_data, asimov_nom_gammas

def hypotest(obs_data, model, poi, observables, backend):
    """
    Find the asimov data and perform the hypothesis test itself to find the CLs values
    for the observed data and the expected values.
    """
    # Generate the asimov data from the given pdf. A fit is performed with fixed mu to find best-fit values
    #  for the other parameter. If this fit is not performed, it is called having nominal asimov data.
    asimov_data, asimov_nom_gammas = post_fit_asimov(observables, model, obs_data, poi, 0, backend)

    # Compute the confidence levels of excluding mu = 1 based on the observed or asimov data
    return get_CLs(model, obs_data, asimov_data, poi, asimov_nom_gammas, backend)

def test_hypotest(benchmark, backend_RooFit, workspace):
    ws = read_workspace(workspace+"_ROOT.json")

    model = ws["ModelConfig"]
    mu = model.GetParametersOfInterest()[0]
    mu.setRange(0, 10)
    mu.setVal(1.0)  # Setting the signal strength for which the CLs values are calculated.
    observables = model.GetObservables()
    obs_data = ws.data("obsData")

    result = benchmark(hypotest, obs_data, model, mu, observables, backend_RooFit)
    benchmark.extra_info["CLs_obs"] = result[0]
    benchmark.extra_info["CLs_exp"] = result[1]

def main(workspace, backend):
    ws = read_workspace(workspace+"_ROOT.json")

    model = ws["ModelConfig"]
    mu = model.GetParametersOfInterest()[0]
    mu.setRange(0, 10)
    mu.setVal(1.0)
    observables = model.GetObservables()
    obs_data = ws.data("obsData")

    CLs_obs, CLs_exp = hypotest(obs_data, model, mu, observables, backend)
    print(f"Observed: {CLs_obs}, Expected: {CLs_exp}")


if __name__ == "__main__":
    main("./models/workspace00", "off")
