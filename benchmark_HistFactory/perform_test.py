out_name = 'compare_backends'
group_by = 'param:workspace'
timer = None #'time.process_time'
histogram_name = None #'./.benchmarks/benchmark_'`date +%F_%H%M`

workspace_list = [
#    './models/workspace00',
#    './models/workspace01',
#    './models/workspace02',
#    './models/workspace03',
#    './models/workspace04',
#    './models/workspace05',
#    './models/workspace06',
#    './models/workspace07',
#    './models/workspace10',
#    './models/workspace11',
#    './models/workspace12',
#    './models/workspace13',
#    './models/workspace14',
#    './models/workspace15',
#    './models/workspace16',
#    './models/workspace30',
#    './models/workspace31',
#    './models/workspace32',
#    './models/workspace33',
#    './models/workspace34',
    './models/workspace40',
    './models/workspace41',
    './models/workspace42',
    './models/workspace43',
    './models/workspace44',
    './models/workspace45',
#    './models/workspace46'
]
backend_pyhf_list = [
#    'numpy',
#    'pytorch',
#    'jax',
    'tensorflow'
]
backend_RooFit_list = [
#    'legacy',
#    'cpu',
#    'codegen'
]

def get_command():
    command = 'python -m pytest'
    if out_name is not None:
        command += ' --benchmark-save='+out_name
    if histogram_name is not None:
        command += ' --benchmark-histogram='+histogram_name
    if group_by is not None:
        command += ' --benchmark-group-by='+group_by
    if timer is not None:
        command += ' --benchmark-timer='+timer
    for workspace in workspace_list:
        command += ' --workspace='+workspace
    for backend in backend_pyhf_list:
        command += ' --backend_pyhf='+backend
    for backend in backend_RooFit_list:
        command += ' --backend_RooFit='+backend
    return command

def main():
    import os
    command = get_command()
    os.system(command)

if __name__ == "__main__":
    main()
