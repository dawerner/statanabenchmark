trim_json() {
	BenchmarkDir=$1
	OutDir=$2

	StartDir=$(pwd)
	mkdir -p $OutDir

	cd $BenchmarkDir
	for File in `find -name '*.json'` # Iterate over all benchmarks saved as json files 
	do
		FileName=${File##*/}						# Change the path in the filename to the starting
		OutFile=$StartDir'/'$OutDir'/'$FileName		#  directory and change the extension to .log to
		sed '/benchmarks/,/^    \]/!d' $File > $OutFile
		sed -i 's/\"benchmarks\": //g' $OutFile
		sed -i 's/^    ],$/]/g' $OutFile
	done

	cd $StartDir
}

trim_json ../benchmark_pyhf/.benchmarks/ ./outputs/pyhf
trim_json ../benchmark_zfit/.benchmarks/ ./outputs/zfit
