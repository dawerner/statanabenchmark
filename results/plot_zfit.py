def import_result(filename):
    import pandas as pd
    import numpy as np
    json_frame = pd.read_json(filename, orient="records")

    results = [json_frame["extra_info"][i] for i in json_frame.index]
    params = [json_frame["params"][i] for i in json_frame.index]
    stats = [json_frame["stats"][i] for i in json_frame.index]

    mu_value_list = [result["mu"] for result in results[:]]
    mu_error_list = [result["mu_err"] if "mu_err" in result else 0 for result in results[:]]

    n_events_list = np.array([params_dict["n_events"] for params_dict in params])
    model_name_list = np.array([params_dict["model_details"][0]+f"{'_'+str(params_dict['model_details'][1]) if params_dict['model_details'][1] is not None else ''}" for params_dict in params])
    backend_list = np.array(
            [
                f"RooFit {params_dict['backend_RooFit']}" if "backend_RooFit" in params_dict
                else f"zfit {params_dict['device_name'][:-2]} {params_dict['use_grad']} {'graph' if params_dict['use_graph'] else 'no_graph'}"
                for params_dict in params
                ]
            )
    device_list = np.array(
        [
            params_dict["device_name"] if "device_name" in params_dict
            else "GPU:0" if params_dict["backend_RooFit"] == "cuda"
            else "CPU:0"
            for params_dict in params
        ]
    )

    df = pd.DataFrame.from_dict(stats[:])
    df["n_events"] = n_events_list
    df["model_name"] = model_name_list
    df["backend"] = backend_list
    df["device"] = device_list
    df["mu_value"] = mu_value_list
    df["mu_error"] = mu_error_list

    return df

def link_results(file_list):
    import pandas as pd
    dataframe_list = []
    for file in file_list:
        df_temp = import_result(file)
        dataframe_list.append(df_temp)

    dataframe = pd.concat(dataframe_list, ignore_index=True)

    return dataframe

def get_colors_from_ROOT(color_names):
    import ROOT

    color_list = []
    for name in color_names:
        color_list.append(ROOT.gROOT.GetColor(name).AsHexString())

    return color_list

def plot_errorbars(df, observable, output_name):
    import numpy as np
    import matplotlib.pyplot as plt
    import ROOT

    unique_backend_names = sorted(set(df["backend"]))
    unique_events = sorted(set(df["n_events"]))

    colors_RooFit = get_colors_from_ROOT([ROOT.kPink-1, ROOT.kRed, ROOT.kOrange+7, ROOT.kOrange])
    colors_zfit = get_colors_from_ROOT([ROOT.kViolet+8, ROOT.kBlue+1, ROOT.kAzure-3, ROOT.kAzure+8])

    fig, (ax0, ax1) = plt.subplots(2,1, sharex=True, figsize=(8,6), gridspec_kw={'height_ratios': [4,1]})

    for backend in unique_backend_names:
        sub_frame = df.loc[df["backend"] == backend].sort_values("n_events")
        color = colors_zfit.pop(0) if "zfit" in backend else colors_RooFit.pop(0)
        ax0.errorbar(x=sub_frame["n_events"], y=sub_frame["mean"], yerr=sub_frame["stddev"], label=backend,
                    elinewidth=1, capsize=4, marker="o", ms=4, linewidth=1, linestyle="-.", color=color)

    ax0.grid(True)
    ax0.tick_params(which="both", bottom=True, top=True, left=True, right=True)
    ax0.set_yscale("log")
    ax0.set_xscale("log")
    ax0.set_ylim(5e-4, 3e2)
    ax0.set_ylabel("wall time in s")
    ax0.legend(ncol=2)

    mu_ratios = {}
    mu_ratios_err = {}

    for events in unique_events:
        sub_frame = df.loc[df["n_events"] == events]
        sub_frame_zfit = sub_frame.loc[sub_frame["backend"].str.contains("zfit")]
        sub_frame_RooFit = sub_frame.loc[sub_frame["backend"].str.contains("RooFit")]

        mu_values = [sub_frame_RooFit["mu_value"].to_numpy(float), sub_frame_zfit["mu_value"].to_numpy(float)]
        mu_errors = [sub_frame_RooFit["mu_error"].to_numpy(float), sub_frame_zfit["mu_error"].to_numpy(float)]
        mu_means = [np.sum(values/errors**2)/np.sum(1/errors**2) if min(errors)>0 else np.mean(values) for values, errors in zip(mu_values, mu_errors)]
        mu_means_err = [np.sqrt(1/np.sum(1/errors**2)) if min(errors)>0 else np.array(0) for errors in mu_errors]

        if mu_means[0] == 0 or mu_means[1] == 0:
            mu_ratio = 1
            mu_ratio_err = 0
        else:
            mu_ratio = mu_means[0]/mu_means[1]
            mu_ratio_err = np.sqrt(mu_means_err[0]**2 * (1/mu_means[1])**2 + mu_means_err[1]**2 * (mu_means[0]/mu_means[1]**2)**2)

        mu_ratios[events] = mu_ratio
        mu_ratios_err[events] = mu_ratio_err

    ax1.plot([0, 1.5*max(unique_events)], [1,1], color="red", linestyle="dotted", linewidth=0.7)
    ax1.errorbar(x=unique_events, y=[mu_ratios[events] for events in unique_events], yerr=[mu_ratios_err[events] for events in unique_events],
                 elinewidth=1, capsize=4, marker="o", ms=4, linewidth=0, color="k")

#    ax1.set_ylim(0.95, 1.05)
    ax1.set_ylabel("Ratio $\mu_{RooFit}/\mu_{zfit}$")
    ax1.set_xlim(100, 1.3*max(unique_events))
    ax1.set_xlabel("Number $n$ of "+observable)

    plt.savefig("./plots/zfit/"+output_name+".pdf")
    plt.savefig("./plots/zfit/"+output_name+".png")

def main():
    from pathlib import Path
    import glob
    import re

    Path("./plots/zfit").mkdir(parents=True, exist_ok=True)

    file_list = glob.glob("./outputs/zfit/*.json")
    dataframe = link_results(file_list)

    model_names = set(dataframe["model_name"])
    for model_name in model_names:
        df = dataframe.loc[dataframe["model_name"] == model_name]
        df_cpu = df.loc[df["device"] == "CPU:0"]
        df_gpu = df.loc[df["device"] == "GPU:0"]
        if(len(df_cpu)):
            plot_errorbars(df_cpu, "Events", "zfit_"+model_name+"_cpu")
        if(len(df_gpu)):
            plot_errorbars(df_gpu, "Events", "zfit_"+model_name+"_gpu")

if __name__ == "__main__":
    main()
