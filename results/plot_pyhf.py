def import_result(filename):
    import pandas as pd
    import re
    json_frame = pd.read_json(filename, orient="records")

    results = [json_frame["extra_info"][i] for i in json_frame.index]
    params = [json_frame["params"][i] for i in json_frame.index]
    stats = [json_frame["stats"][i] for i in json_frame.index]

    CLs_obs_list = [result["CLs_obs"] for result in results[:]]
    CLs_exp_list = [result["CLs_exp"] for result in results[:]]

    workspace_names = [re.search(r'workspace\d\d', params_dict["workspace"]).group() for params_dict in params]
    backend_names = [
        f"RooFit {params_dict['backend_RooFit']}" if "backend_RooFit" in params_dict
        else f"pyhf {params_dict['backend_pyhf']}"
        for params_dict in params
    ]

    df = pd.DataFrame.from_dict(stats[:])
    df["workspace"]=workspace_names
    df["backend"]=backend_names
    df["CLs_obs"]=CLs_obs_list
    df["CLs_exp"]=CLs_exp_list

    comparison_dict = {
        "workspace00":"bins_1",
        "workspace01":"bins_1",
        "workspace02":"bins_1",
        "workspace03":"bins_1",
        "workspace04":"bins_1",
        "workspace05":"bins_1",
        "workspace06":"bins_1",
        "workspace07":"bins_1",
        "workspace10":"bins_2",
        "workspace11":"bins_2",
        "workspace12":"bins_2",
        "workspace13":"bins_2",
        "workspace14":"bins_2",
        "workspace15":"bins_2",
        "workspace16":"bins_2",
        "workspace30":"samples",
        "workspace31":"samples",
        "workspace32":"samples",
        "workspace33":"samples",
        "workspace34":"samples",
        "workspace40":"channels",
        "workspace41":"channels",
        "workspace42":"channels",
        "workspace43":"channels",
        "workspace44":"channels",
        "workspace45":"channels",
        "workspace46":"channels"
    }
    positions_dict = {
        "workspace00":1,
        "workspace01":2,
        "workspace02":4,
        "workspace03":8,
        "workspace04":16,
        "workspace05":32,
        "workspace06":64,
        "workspace07":128,
        "workspace10":1,
        "workspace11":2,
        "workspace12":4,
        "workspace13":8,
        "workspace14":16,
        "workspace15":32,
        "workspace16":64,
        "workspace30":1,
        "workspace31":2,
        "workspace32":3,
        "workspace33":4,
        "workspace34":5,
        "workspace40":1,
        "workspace41":2,
        "workspace42":4,
        "workspace43":8,
        "workspace44":16,
        "workspace45":32,
        "workspace46":64
    }
    df["comparison"] = [comparison_dict[key] for key in df["workspace"]]
    df["positions"] = [positions_dict[key] for key in df["workspace"]]

    return df

def link_results(file_list):
    import pandas as pd
    dataframe_list = []
    for file in file_list:
        df_temp = import_result(file)
        dataframe_list.append(df_temp)

    dataframe = pd.concat(dataframe_list, ignore_index=True)
    return dataframe

def get_colors_from_ROOT(color_names):
    import ROOT

    color_list = []
    for name in color_names:
        color_list.append(ROOT.gROOT.GetColor(name).AsHexString())

    return color_list

def plot_errorbars(df, observable, output_name, xlims):
    import numpy as np
    import matplotlib.pyplot as plt
    import ROOT

    unique_backend_names = sorted(set(df["backend"]))
    unique_positions = sorted(set(df["positions"]))

    colors_RooFit = get_colors_from_ROOT([ROOT.kPink-1, ROOT.kRed, ROOT.kOrange+7])
    colors_pyhf = get_colors_from_ROOT([ROOT.kViolet+8, ROOT.kBlue+1, ROOT.kAzure-3, ROOT.kAzure+8])

    fig, (ax0, ax1) = plt.subplots(2,1, sharex=True, figsize=(7,5.25), gridspec_kw={'height_ratios': [4,1]})

    for backend in unique_backend_names:
        sub_frame = df.loc[df["backend"] == backend].sort_values("positions")
        color = colors_pyhf.pop(0) if "pyhf" in backend else colors_RooFit.pop(0)
        ax0.errorbar(x=sub_frame["positions"], y=sub_frame["mean"], yerr=sub_frame["stddev"], label=backend,
                    elinewidth=1, capsize=4, marker="o", ms=4, linewidth=1, linestyle="-.", color=color)

    ax0.grid(True)
    ax0.tick_params(which="both", bottom=True, top=True, left=True, right=True)
    ax0.set_yscale("log")
    ax0.set_xscale("log")
    ax0.set_ylim(3e-3, 7e2)
    ax0.set_ylabel("wall time in s")
    ax0.legend(loc=2, ncol=2)

    CLs_obs_ratios = {}
    CLs_exp_ratios = {}

    for position in unique_positions:
        sub_frame = df.loc[df["positions"] == position]
        sub_frame_pyhf = sub_frame.loc[sub_frame["backend"].str.contains("pyhf")]
        sub_frame_RooFit = sub_frame.loc[sub_frame["backend"].str.contains("RooFit")]

        CLs_obs_means = [sub_frame_RooFit["CLs_obs"].mean(), sub_frame_pyhf["CLs_obs"].mean()]
        CLs_exp_means = [sub_frame_RooFit["CLs_exp"].mean(), sub_frame_pyhf["CLs_exp"].mean()]
        CLs_obs_ratios[position] = CLs_obs_means[0]/CLs_obs_means[1] if CLs_obs_means[0] and CLs_obs_means[1] else 1
        CLs_exp_ratios[position] = CLs_exp_means[0]/CLs_exp_means[1] if CLs_exp_means[0] and CLs_exp_means[1] else 1

    ax1.plot([0, 1.5*max(unique_positions)], [1,1], color="red", linestyle="dotted", linewidth=0.5, zorder=0)
    ax1.scatter(x=[position for position in unique_positions], y=[CLs_obs_ratios[position] for position in unique_positions], label="CLs_obs",
                linewidth=0, marker="o")
    ax1.scatter(x=[position for position in unique_positions], y=[CLs_exp_ratios[position] for position in unique_positions], label="CLs_exp",
                linewidth=0, marker="^", s=20)

    ax1.set_ylim(0.98, 1.02)
    ax1.set_xlim(xlims[0], xlims[1])
    ax1.set_ylabel("Ratio $CLs_{RooFit}/CLs_{pyhf}$")
    ax1.set_xlabel("Number $n$ of "+observable)
    ax1.legend(ncol=2)

    plt.savefig("./plots/pyhf/"+output_name+".pdf")
    plt.savefig("./plots/pyhf/"+output_name+".png")

def main():
    from pathlib import Path
    import glob

    Path("./plots/pyhf").mkdir(parents=True, exist_ok=True)

    file_list = glob.glob("./outputs/pyhf/*.json")
    dataframe = link_results(file_list)

    df_bin_comp = dataframe.loc[dataframe["comparison"] == "bins_1"]
    plot_errorbars(df_bin_comp, "bins", "bin_comparison_1_channel", (0.9, 140))
    df_bin_comp = dataframe.loc[dataframe["comparison"] == "bins_2"]
    plot_errorbars(df_bin_comp, "bins", "bin_comparison_2_channels", (0.9, 140))
    df_sample_comp = dataframe.loc[dataframe["comparison"] == "samples"]
    plot_errorbars(df_sample_comp, "samples", "sample_comparison", (0.9, 5.5))
    df_channel_comp = dataframe.loc[dataframe["comparison"] == "channels"]
    plot_errorbars(df_channel_comp, "channels", "channel_comparison", (0.9, 70))

if __name__ == "__main__":
    main()
