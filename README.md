# Document Title

## Quickstart guide - How to run the benchmarks
In this workflow the packages pyhf and zfit are benchmarked against RooFit.
These two packages and all other required packages are listed in ``requirements.txt`` and can be installed with the following command:
```
pip install -r requirements.txt
```
NOTE: When trying to run tensorflow on a GPU (e.g. in the ``pyhf`` backend or in ``zfit``), tensorflow must support the cuda version.
In addition, the master branch of ROOT must be installed (e.g. via this guide https://root.cern/install/build_from_source/ by cloning ``master`` instead of ``latest-stable``).
If the branch ``latest-stable`` is used instead, some of the RooFit backends may not be available.

To run the benchmarking workflow, first the workspaces need to be created.
This is performed by running the script ``createWorkspaces.py``:
```
cd benchmark_Hypotest
python createWorkspace.py
```
The program then reads the file ``config.json`` that specifies the names of the workspaces, the bin numbers, and numbers and names of the channels and their samples.
In the folder ``models/`` all workspaces are saved as ``*.json`` files that can be read by the benchmarked code.

To run the benchmarks, just execute the script ``perform_test.py``:
```
python perform_test.py
```
Inside the script, the workspaces can be toggled by commenting or uncommenting them in the workspace list.
The ``pyhf`` package has multiple available backends that can all be benchmarked. 
To only benchmark some backends, the others need to be toggled from the backend list.
Similarly, RooFit has multiple backends that can be benchmarked and toggled.

Many of these workspaces are turned of by default so that the default benchmark is executed fast.
Executing the benchmark multiple times with different workspaces works fine and produces output plots with all benchmarked workspaces.

The benchmarks of zfit can be run by executing the following commands:
```
cd ../benchmark_unbinned
python perform_test.py
```
Here, the ``perform_test.py`` script is used again for turning backends of RooFit on and off and for varying the complexity of the fit.
For zfit multiple options are specified:
- The fits can either be performed on a cpu or (if available) on a gpu (this is turned off by default).
- The computation of the gradient can either be performed by tensorflow (``grad``) or by iminuit (``no_grad``).
- Tensorflow can run in eager mode (``use_graph=0``) or use the computation graph (``use_graph=1``).
All 8 possible combinations of these options will be performed if all are activated.
Currently there is a problem with memory usage in the ``zfit`` benchmark (probably due to models and data not being cleared after the fit). This leads to out-of-memory errors if too many of the benchmarks are executed at once.

The results of the benchmarks are printed to the standard output after the benchmark finishes.
To further analyse and plot these results, the folder ``results/`` contains some scripts that can be executed:
```
cd ../results
./readOutput.sh
python plot_pyhf.py
python plot_zfit.py
```
The first script to call is ``readOutput.sh`` that copies and trims the json outputs from the hidden folder ``benchmark_*/.benchmarks``.
Afterwards the script ``plotting.py`` can be called to create plots comparing the benchmarking results from several backends and workspaces.
This script reads all ``json`` files into a pandas dataframe and then plots the three comparisons with growing numbers of bins, samples and channels.

## About this project
This repository is part of my project as a CERN summer student 2023.
The goal is to benchmark different frameworks performing statistical analyses.


### Comparison of template histogram fits
To begin this project the python package ``pyhf`` https://github.com/scikit-hep/pyhf/ is used to analyse template histograms.
The methods in ``pyhf`` are largely based on the statistical analysis paper https://arxiv.org/pdf/1007.1727v3.pdf.
These methods are recreated in pyROOT, using RooFit datatypes.
To perform the benchmark different workspaces are created in the folder ``complexModels``. 
The dimensions of the workspaces are specified by a config file and the bins are filled with randomly generated numbers.
A self-made script writes the workspaces in a JSON format to be read by pyhf and RooFit respectively.
For RooFit the HEP Statistics Serialization Standard (HS3) is used.
As this standard is still work in progress it is only available on the master branch of ROOT (as of August 2023).


### Comparison of unbinned fits
To perform fits of unbinned data to analytical probability distribution functions (pdfs), the python package ``zfit`` https://github.com/zfit/zfit can be used.
Recreating the respective fits in RooFit is much more straightforward than for ``pyhf`` and the results can be seen in the directory ``benchmark_zfit``.
The pdfs used in this benchmark are based on the Master Thesis of Jonas Eschle and the corresponding paper https://arxiv.org/abs/1910.13429 where a similar benchmark was performed in 2019.

### Benchmarking
The benchmarking itself is performed using the package pytest-benchmark.
This package allows to use decorators to make functions benchmarkable and to systematically pass different parameters to the functions when benchmarking for different parameter combinations.
In the result, all backends and options of pyhf, zfit, and RooFit are benchmarked in combination with all different models.
The timer used by pytest-bench can be chosen to either be a performance counter that includes sleep times or to be a counter that ignores these waiting periods to yield the pure computing time.
The latter is usefull for an unbiased comparison of code efficiency, while the former is more useful to users who want to estimate the real duration their code will take.
